extends KinematicBody2D

export (int) var speed = 400
export (int) var run_speed = 700
export (int) var jump_speed = -600
export (int) var GRAVITY = 2000

var can_double_jump = false
const UP = Vector2(0,-1)
export (Vector2) var velocity = Vector2()

export(Texture) var jump_sprite
export(Texture) var walk_sprite
export(Texture) var run_sprite
export(Texture) var default_sprite
onready var sprite = $Sprite

func get_input():
	velocity.x = 0
	#jump
	if can_double_jump and Input.is_action_just_pressed('up'):
		velocity.y = jump_speed
		can_double_jump = false
		sprite.texture = jump_sprite
	elif Input.is_action_just_pressed('up') and is_on_floor():
		can_double_jump = true
		velocity.y = jump_speed
		sprite.texture = jump_sprite
		
	#run
	elif Input.is_key_pressed(KEY_SHIFT) and Input.is_action_pressed('right'):
		velocity.x += run_speed
		sprite.set_flip_h(true)
		sprite.texture = run_sprite
	elif Input.is_key_pressed(KEY_SHIFT) and Input.is_action_pressed('left'):
		velocity.x -= run_speed
		sprite.set_flip_h(false)
		sprite.texture = run_sprite
		
	#move
	elif Input.is_action_pressed('right'):
		velocity.x += speed
		sprite.set_flip_h(false)
		sprite.texture = walk_sprite
	elif Input.is_action_pressed('left'):
		velocity.x -= speed
		sprite.set_flip_h(true)
		sprite.texture = walk_sprite
		
	#default
	elif is_on_floor():
		sprite.texture = default_sprite
		
func _physics_process(delta):
	velocity.y += delta * GRAVITY #delta there is usually the amount of time it took for the last frame to be processed
	get_input()
	velocity = move_and_slide(velocity, UP)
